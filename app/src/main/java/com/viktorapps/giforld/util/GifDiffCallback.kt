package com.viktorapps.giforld.util

import androidx.recyclerview.widget.DiffUtil

class GifDiffCallback<E>(
        var oldItems: List<E>,
        var newItems: List<E>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldItems.size
    override fun getNewListSize() = newItems.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldItems[oldItemPosition].hashCode() == newItems[newItemPosition].hashCode()

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldItems[oldItemPosition]!! == newItems[newItemPosition]
}