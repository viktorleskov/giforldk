package com.viktorapps.giforld.util

interface ChangeShareIntentListener {
    fun onChangeShareIntent(text: String)
}