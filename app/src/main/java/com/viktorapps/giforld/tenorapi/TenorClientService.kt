package com.viktorapps.giforld.tenorapi

import android.os.StrictMode
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.viktorapps.giforld.tenorapi.model.TenorRequestParams
import com.viktorapps.giforld.tenorapi.model.TenorResponse
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStreamReader
import java.io.StringWriter
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.URL

class TenorClientService {

    companion object Control {
        private var tenorStaticAccess: TenorClientService? = null
        private var requesterSet: HashSet<TenorRequester>? = null
        private var lastSearchTerm: String? = null
        private val TENOR_SEARCH_QUERY = "https://api.tenor.com/v1/random?q=%1\$s&key=%2\$s&limit=%3\$s&pos=%4\$s"
        private val API_KEY = "78L5HGR4Z7MV"
        private var nextValue = ""

        val DEFAULT_SEARCH_LIMIT = 10
        val DEFAULT_SEARCH_TERM = "excited"

        fun getAccess(): TenorClientService {
            if (tenorStaticAccess == null) {
                tenorStaticAccess = TenorClientService()
            }
            return tenorStaticAccess as TenorClientService
        }
    }

    fun requestTenorUpdate(requester: TenorRequester, tenorRequestParams: TenorRequestParams) {
        when (registerRequester(requester)) {
            true -> {
                GlobalScope.launch {
                    val response = async { TenorCLient().getResponse(tenorRequestParams) }
                    onResponseForAllRequester(response.await())
                }
            }
            else -> return
        }

    }

    private fun onResponseForAllRequester(response: TenorResponse?) {
        if (response != null) {
            for (requester in requesterSet!!) {
                requester.onTenorReturnResponse(response)
            }
        }
        requesterSet?.clear()
    }

    private fun registerRequester(requester: TenorRequester): Boolean {
        if (requesterSet == null) {
            requesterSet = LinkedHashSet()
        }
        return if (requesterSet!!.contains(requester)) {
            false;
        } else {
            requesterSet!!.add(requester)
            true;
        }
    }

    class TenorCLient {
        fun getResponse(params: TenorRequestParams): TenorResponse? {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
            val searchTerm = params.searchTerm
            if (searchTerm != lastSearchTerm) nextValue = ""
            lastSearchTerm = searchTerm
            return getTenorSearchResult(lastSearchTerm!!, params.searchLimit)
        }

        private fun getTenorSearchResult(searchTerm: String, limit: Int): TenorResponse? {
            val tenorResponse: TenorResponse
            val gson = Gson()
            val searchResult: JSONObject? = getSearchResult(searchTerm, limit)
            try {
                tenorResponse = gson.fromJson(searchResult.toString(), TenorResponse::class.java)
                return tenorResponse
            } catch (e: JsonSyntaxException) {
                e.printStackTrace()
            }
            return null
        }

        private fun getSearchResult(searchTerm: String, limit: Int): JSONObject? {
            val url = String.format(TENOR_SEARCH_QUERY,
                    searchTerm, API_KEY, limit, nextValue)
            try {
                Log.v("Request URL :", url)
                return get(url)
            } catch (ignored: IOException) {
            } catch (ignored: JSONException) {
            }
            return null
        }

        @Throws(IOException::class, JSONException::class)
        private operator fun get(url: String): JSONObject? {
            var connection: HttpURLConnection? = null
            try {
                connection = URL(url).openConnection() as HttpURLConnection
                connection.doInput = true
                connection.doOutput = true
                connection.requestMethod = "GET"
                connection.setRequestProperty("Content-Type", "application/json")
                connection.setRequestProperty("Accept", "application/json")
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
                val statusCode = connection.responseCode
                if (statusCode != HttpURLConnection.HTTP_OK && statusCode != HttpURLConnection.HTTP_CREATED) {
                    val error = String.format("HTTP Code: '%1\$s' from '%2\$s'", statusCode, url)
                    throw ConnectException(error)
                }
                return parser(connection)
            } catch (ignored: Exception) {
            } finally {
                connection?.disconnect()
            }
            return JSONObject("")
        }

        @Throws(JSONException::class)
        private fun parser(connection: HttpURLConnection?): JSONObject? {
            val buffer = CharArray(1024 * 4)
            var n: Int
            try {
                BufferedInputStream(connection!!.inputStream).use { stream ->
                    val reader = InputStreamReader(stream, "UTF-8")
                    val writer = StringWriter()
                    while (-1 != reader.read(buffer).also { n = it }) {
                        writer.write(buffer, 0, n)
                    }
                    return JSONObject(writer.toString())
                }
            } catch (ignored: IOException) {
            }
            return JSONObject("")
        }
    }
}