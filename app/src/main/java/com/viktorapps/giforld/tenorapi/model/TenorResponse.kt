package com.viktorapps.giforld.tenorapi.model

data class TenorResponse(
        var weburl: String,
        val results: List<Result>,
        val next: String
)