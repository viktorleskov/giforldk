package com.viktorapps.giforld.tenorapi.model

data class TenorRequestParams(
        val searchTerm: String,
        val searchLimit: Int
)