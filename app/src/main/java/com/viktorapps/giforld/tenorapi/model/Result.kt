package com.viktorapps.giforld.tenorapi.model

data class Result(
        var hascaption: Boolean,
        var tags: List<String?>,
        var url: String,
        var media: List<Media?>,
        var created: Double,
        var shares: Long,
        var itemurl: String,
        var composite: Any,
        var hasaudio: Boolean,
        var title: String,
        var id: Long
)