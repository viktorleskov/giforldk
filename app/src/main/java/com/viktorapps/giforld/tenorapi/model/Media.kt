package com.viktorapps.giforld.tenorapi.model

data class Media(
        var nanomp4: MediaItem,
        var nanowebm: MediaItem,
        var tinygif: MediaItem,
        var tinymp4: MediaItem,
        var tinywebm: MediaItem,
        var webm: MediaItem,
        var gif: MediaItem,
        var mp4: MediaItem,
        var loopedmp4: MediaItem,
        var mediumgif: MediaItem,
        var nanogif: MediaItem?
)