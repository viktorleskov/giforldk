package com.viktorapps.giforld.tenorapi

import com.viktorapps.giforld.tenorapi.model.TenorResponse

interface TenorRequester {
    fun onTenorReturnResponse(tenorResponse: TenorResponse)
}