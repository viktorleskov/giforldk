package com.viktorapps.giforld.tenorapi.model

data class MediaItem(
        var url: String,
        var dims: Array<Int>,
        var duration: Double,
        var preview: String,
        var size: Long? = null
)