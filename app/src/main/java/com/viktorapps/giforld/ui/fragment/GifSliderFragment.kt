package com.viktorapps.giforld.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.viktorapps.giforld.R
import com.viktorapps.giforld.tenorapi.TenorClientService.Control.DEFAULT_SEARCH_TERM
import com.viktorapps.giforld.tenorapi.TenorClientService.Control.getAccess
import com.viktorapps.giforld.tenorapi.TenorRequester
import com.viktorapps.giforld.tenorapi.model.TenorRequestParams
import com.viktorapps.giforld.tenorapi.model.TenorResponse
import com.viktorapps.giforld.ui.MainActivity.Companion.menu
import com.viktorapps.giforld.util.ChangeShareIntentListener
import com.viktorapps.giforld.util.GifDiffCallback
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class GifSliderFragment : Fragment(), TenorRequester, SearchView.OnQueryTextListener {
    private var sliderItems: List<SliderItem> = ArrayList<SliderItem>()
    private var sliderAdapter: SliderAdapter? = null
    private lateinit var viewPager2: ViewPager2
    private val DEFAULT_TENOR_REQUESTER: TenorRequester = this
    private var onChangeShareIntentListener: ChangeShareIntentListener? = null
    private val sliderPrivateIOMutex = Any()

    companion object {
        private val OFFSET_SLIDER_ITEMS_COUNT: Int = 16
        private var SEARCH_TERM = DEFAULT_SEARCH_TERM
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.gif_slider_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requestTenorNewImages(null, null)
        viewPager2 = view.findViewById(R.id.viewPagerImageSlider)
        sliderAdapter = SliderAdapter(sliderItems, viewPager2)
        sliderAdapter!!.registerAdapterDataObserver(object : AdapterDataObserver() {
            override fun onChanged() {
                Log.v("keko ", " on changed")
                super.onChanged()
            }
        })
        viewPager2.setAdapter(sliderAdapter)
        //attributes
        viewPager2.setOffscreenPageLimit(3)
        viewPager2.getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER

        //transformer
        viewPager2.setPageTransformer(
                this.buildPageTransformer()
        )
        //callbacks
        viewPager2.registerOnPageChangeCallback(
                buildOnPageChangesCallListener()
        )
        super.onViewCreated(view, savedInstanceState)
    }

    private fun buildOnPageChangesCallListener(): OnPageChangeCallback {
        return object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if (sliderItems.size > OFFSET_SLIDER_ITEMS_COUNT * 4 && position > sliderItems.size / 2) {
                    deleteFirstSliderElements(OFFSET_SLIDER_ITEMS_COUNT)
                }
                if (sliderItems[position].url != null) {
                    onChangeShareIntentListener!!.onChangeShareIntent(sliderItems[position].url!!)
                }
                if ((position + 2 > sliderItems.size
                                && sliderItems.size < OFFSET_SLIDER_ITEMS_COUNT * 2)
                        || sliderItems.size - position == OFFSET_SLIDER_ITEMS_COUNT) {
                    requestTenorNewImages(null, null)
                }
                super.onPageSelected(position)
            }
        }
    }

    private fun requestTenorNewImages(requester: TenorRequester?, params: TenorRequestParams?) {
        getAccess()
                .requestTenorUpdate(
                        requester ?: DEFAULT_TENOR_REQUESTER,
                        params
                                ?: TenorRequestParams(SEARCH_TERM, OFFSET_SLIDER_ITEMS_COUNT))
    }

    private fun buildPageTransformer(): CompositePageTransformer? {
        val compositePageTransformer = CompositePageTransformer()
        compositePageTransformer.addTransformer(MarginPageTransformer(1))
        return compositePageTransformer
    }

    override fun onTenorReturnResponse(tenorResponse: TenorResponse) {
        addNewSliderElements(tenorResponse)
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        SEARCH_TERM = query
        val searchMenuItem = menu.findItem(R.id.action_search)
        val searchView = searchMenuItem.actionView as SearchView
        searchView.queryHint = query
        searchMenuItem.collapseActionView()
        viewPager2.setCurrentItem(sliderItems.size, true)
        requestTenorNewImages(
                null,
                null)
        return false
    }

    fun setOnChangeShareIntentListener(target: ChangeShareIntentListener?) {
        onChangeShareIntentListener = target
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }

    private fun addNewSliderElements(response: TenorResponse) {
        GlobalScope.launch(Dispatchers.IO) {
            val bufList: ArrayList<SliderItem> = ArrayList(sliderItems)
            response.results.forEach { result ->
                result.media.forEach { media ->
                    bufList.add(SliderItem(media?.gif?.url!!))
                }
            }
            withContext(Dispatchers.Main){
                updateAdapterDataList(bufList)
            }
        }
    }

    private fun deleteFirstSliderElements(countToDelete: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            val bufList: MutableList<SliderItem> = ArrayList(sliderItems)
            bufList.removeIf { sliderItem -> bufList.indexOf(sliderItem) < countToDelete }
            withContext(Dispatchers.Main){
                updateAdapterDataList(bufList)
            }
        }
    }

    private fun updateAdapterDataList(newList: List<SliderItem>) {
        val needToScroll = sliderItems.size == 0
        val diffResult = DiffUtil.calculateDiff(GifDiffCallback(sliderItems, newList), true)
        sliderAdapter!!.setData(newList)
        sliderItems = newList
        diffResult.dispatchUpdatesTo(sliderAdapter!!)
        if (needToScroll) viewPager2.setCurrentItem(1, true)
    }
}