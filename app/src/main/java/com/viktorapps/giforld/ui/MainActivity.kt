package com.viktorapps.giforld.ui

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.view.Menu
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.ShareActionProvider
import androidx.appcompat.widget.Toolbar
import androidx.core.view.MenuItemCompat
import androidx.fragment.app.FragmentTransaction
import com.viktorapps.giforld.R
import com.viktorapps.giforld.ui.fragment.GifSliderFragment
import com.viktorapps.giforld.util.ChangeShareIntentListener

class MainActivity : AppCompatActivity(), ChangeShareIntentListener {
    lateinit var shareActionProvider: ShareActionProvider
    lateinit var transaction: FragmentTransaction
    private lateinit var gifSlider: GifSliderFragment

    companion object {
        lateinit var menu: Menu
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        enterFullScreenFlags()
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().permitAll().build())
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toobar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toobar)
        gifSlider = GifSliderFragment()

        transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.gif_slider_container, gifSlider)
        transaction.commit()

        gifSlider.setOnChangeShareIntentListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        MainActivity.menu = menu!!
        menuInflater.inflate(R.menu.menu_main, menu)
        val menuItem = menu.findItem(R.id.action_share)
        shareActionProvider = MenuItemCompat.getActionProvider(menuItem) as ShareActionProvider
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView.queryHint = resources.getString(R.string.search_hint)
        searchView.setOnQueryTextListener(gifSlider)
        return super.onCreateOptionsMenu(menu)
    }

    fun enterFullScreenFlags() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        window
                .setFlags(
                        WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN
                )
    }

    override fun onChangeShareIntent(text: String) {
        shareActionProvider.refreshVisibility()
        val sendIntent = Intent(Intent.ACTION_SEND)
        sendIntent.type = "text/plain"
        sendIntent.putExtra(Intent.EXTRA_TEXT, text)
        shareActionProvider.setShareIntent(sendIntent)
    }
}