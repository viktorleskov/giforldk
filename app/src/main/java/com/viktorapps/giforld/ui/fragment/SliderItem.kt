package com.viktorapps.giforld.ui.fragment

class SliderItem(
        var url: String?,
        var resourceId: Int,
        var itemId: Long
) {
    constructor(url: String) : this(url, 0, counter++)
    constructor(resource_id: Int) : this(null, resource_id, counter++)

    companion object {
        var counter: Long = 0
    }

    override fun hashCode(): Int {
        return itemId.toInt()
    }

    override fun equals(other: Any?): Boolean {
        return hashCode() == other.hashCode()
    }
}