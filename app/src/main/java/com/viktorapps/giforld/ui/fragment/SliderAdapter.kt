package com.viktorapps.giforld.ui.fragment

import android.graphics.drawable.AnimatedImageDrawable
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.viktorapps.giforld.R

class SliderAdapter(
        private var sliderItems: List<SliderItem>,
        private var viewPager2: ViewPager2
) : RecyclerView.Adapter<SliderAdapter.SliderViewHolder>() {
    init {
        setHasStableIds(true)
    }

    fun setData(newItems: List<SliderItem>) {
        sliderItems = newItems
    }

    override fun getItemId(position: Int): Long {
        return sliderItems[position].itemId
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderAdapter.SliderViewHolder {
        return SliderAdapter.SliderViewHolder(itemView = LayoutInflater.from(parent.context).inflate(
                R.layout.slide_item_container,
                parent,
                false
        ))
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onBindViewHolder(holder: SliderAdapter.SliderViewHolder, position: Int) {
        holder.setImage(sliderItems[position], viewPager2)
    }

    override fun getItemCount(): Int {
        return sliderItems.size
    }

    class SliderViewHolder(
            var imageView: AppCompatImageView? = null, itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        init {
            imageView = itemView.findViewById(R.id.imageSlide)
        }

        @RequiresApi(Build.VERSION_CODES.P)
        fun setImage(sliderItem: SliderItem, viewPager2: ViewPager2) {
            val animPlaceHolder = viewPager2.getContext().getDrawable(R.drawable.giforldlandl) as AnimatedImageDrawable
            animPlaceHolder.start()
            Glide
                    .with(viewPager2)
                    .asGif()
                    .load(if (sliderItem.resourceId != 0) sliderItem.resourceId else sliderItem.url)
                    .placeholder(animPlaceHolder)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .dontTransform()
                    .skipMemoryCache(false)
                    .into(imageView!!)
        }
    }
}